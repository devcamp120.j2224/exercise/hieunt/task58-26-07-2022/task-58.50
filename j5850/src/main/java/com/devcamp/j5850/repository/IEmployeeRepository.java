package com.devcamp.j5850.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j5850.models.CEmployee;

public interface IEmployeeRepository extends JpaRepository <CEmployee, Long> {
    
} 
