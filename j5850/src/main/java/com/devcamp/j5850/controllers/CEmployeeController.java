package com.devcamp.j5850.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j5850.models.CEmployee;
import com.devcamp.j5850.repository.IEmployeeRepository;

@RestController
@CrossOrigin
public class CEmployeeController {
    @Autowired
    IEmployeeRepository employeeRepository;
    @GetMapping("/employeeList")
    public ResponseEntity<List<CEmployee>> getAllEmployees() {
        try {
            List<CEmployee> employeeList = new ArrayList<CEmployee>();
            employeeRepository.findAll().forEach(employeeList::add);
            return new ResponseEntity<List<CEmployee>>(employeeList, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
